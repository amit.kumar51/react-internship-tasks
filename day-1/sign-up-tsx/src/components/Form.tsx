import React from 'react'

function Form() {
    function displayData(e: React.FormEvent<HTMLFormElement>){
        e.preventDefault();
        const form = e.currentTarget;
        const formData = new FormData(form);
        const values = Object.fromEntries(formData.entries());
        console.log(values);
        
    }
  return (
    <div className="main">
      <div className="formWrapper">
        <div className="signUp">
          <h1>Sign Up</h1>
        </div>
        <div className="form">
          <form id="signUpForm" onSubmit={displayData}>
            <div className="inputFields">
              <input type="text" className="inputs" name="firstName" placeholder="First Name" />
            </div>

            <div className="inputFields">
              <input type="text" className="inputs" name="lastName" placeholder="Last Name" />
            </div>

            <div className="inputFields">
              <input type="email" className="inputs" name="email" placeholder="Email" />
            </div>

            <div className="inputFields">
              <input type="password" className="inputs" name="password" placeholder="Password" />
            </div>

            <div className="inputFields">
              <input type="text" className="inputs" name="contact" placeholder="Contact Number" />
            </div>

            <div className="inputFields">
              <input
              type='textarea'
              className="inputs"
                name="address"
                placeholder="Address"
              />
            </div>

            <div className="inputFields">
              <input type="submit" className="inputs btn" name="contact" placeholder="Contact Number" />
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Form