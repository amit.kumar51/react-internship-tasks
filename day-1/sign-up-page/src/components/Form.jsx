import React from "react";

function Form() {
  function displayData(e){
    e.preventDefault();
    const inputs = document.querySelectorAll('input');
    const textarea = document.querySelector('textarea');
    for(let i=0; i<inputs.length-1; i++){
      console.log(inputs[i].value)
    }
    console.log(textarea.value)
  }

  return (
    <div className="main">
      <div className="formWrapper">
        <div className="signUp">
          <h1>Sign Up</h1>
        </div>
        <div className="form">
          <form id="signUpForm">
            <div className="inputFields">
              <input type="text" className="inputs" name="firstName" placeholder="First Name" />
            </div>

            <div className="inputFields">
              <input type="text" className="inputs" name="lastName" placeholder="Last Name" />
            </div>

            <div className="inputFields">
              <input type="email" className="inputs" name="email" placeholder="Email" />
            </div>

            <div className="inputFields">
              <input type="password" className="inputs" name="password" placeholder="Password" />
            </div>

            <div className="inputFields">
              <input type="text" className="inputs" name="contact" placeholder="Contact Number" />
            </div>

            <div className="inputFields">
              <textarea
              className="inputs"
                name="address"
                rows="10"
                cols="17"
                placeholder="Address"
              ></textarea>
            </div>

            <div className="inputFields">
              <input type="submit" className="inputs btn" name="contact" placeholder="Contact Number" onClick={displayData} />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Form;

