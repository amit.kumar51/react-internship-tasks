import styled from "styled-components";

const FormWrapper = styled.div`
  background: #38346b;
  height: 100vh;
  display: flex;
  justify-content: center;
`;
const Heading = styled.h1`
  color: white;
  justify-content: center;
`;
const Form = styled.form`
  margin-top: 50px;
  padding: 30px;
  height: 300px;
  border-radius: 15px;
  box-shadow: 5px 20px 50px #000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const InputWrapper = styled.div`
  margin: 15px 0 15px 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Input = styled.input`
  width: 300px;
  height: 50px;
  border-radius: 5px;
  border: none;
  font-size: 16px;
  padding-left: 10px;
  outline: none;
  box-shadow: 5px 10px 50px #000;
`;

const Button = styled.button`
  background-color: #625da6;
  margin-top: 30px;
  width: 200px;
  height: 40px;
  padding: 8px 16px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  text-decoration: none;
  color: white;
  font-size: 16px;
  font-weight: bold;
`;

export { Form, Input, Button, FormWrapper, InputWrapper, Heading };
