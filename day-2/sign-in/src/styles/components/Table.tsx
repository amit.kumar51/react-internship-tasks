import styled from "styled-components";

export const Table = styled.table`
  margin: auto;
  border-collapse: collapse;
  width: 1400px;
`;

export const Heading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const TableHeader = styled.th`
  padding: 8px;
  text-align: left;
  background-color: #f2f2f2;
  border-bottom: 1px solid #ddd;
`;

export const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f2f2f2;
  }
`;

export const TableData = styled.td`
  padding: 8px;
  border-bottom: 1px solid #ddd;
`;
