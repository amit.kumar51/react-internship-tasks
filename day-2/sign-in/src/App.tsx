import React from 'react';
import { BrowserRouter, Route, Routes} from 'react-router-dom'
import SignIn from 'components/SignIn';
import TableData from 'components/TableData';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
          <Route index element={<SignIn />} />
          <Route path="/table" element={<TableData />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
