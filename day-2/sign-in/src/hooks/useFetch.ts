import React, {useState, useEffect} from 'react'

const useFetch = (url: string) => {
    const [data, setData] = useState([]);

    async function fetchData() {
      try {
        const response = await fetch(
          "https://jsonplaceholder.typicode.com/todos"
        );
        const data = await response.json();
        setData(data);
      } catch (error) {
        console.log(error);
      }
    }
    useEffect(() => {
      fetchData();
    }, []);

    return data;
}

export default useFetch