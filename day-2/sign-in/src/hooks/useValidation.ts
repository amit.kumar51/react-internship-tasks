import { IData } from "interfaces/components/forms/userData";

const validUser: IData = {
  email: "amit@gmail.com",
  password: "1234",
};

const checkValidation = (userData: IData) => {
  if (
    validUser.email === userData.email &&
    validUser.password === userData.password
  ) {
    return true;
  }

  return false;
};

const useValidation = () => checkValidation;

export default useValidation;
