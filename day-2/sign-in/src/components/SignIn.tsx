import useValidation from "hooks/useValidation";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { IData } from "interfaces/components/forms/userData";
import {
  Form,
  Input,
  Button,
  FormWrapper,
  InputWrapper,
  Heading,
} from "styles/components/SignIn";

const SignIn = () => {
  const { register, handleSubmit } = useForm<IData>();
  const navigate = useNavigate();
  const checkValidation = useValidation();

  const getData: SubmitHandler<IData> = (data: IData): any => {
    const isValid = checkValidation(data);
    if (isValid) {
      navigate("./table");
      return;
    }
    alert("Wrong Email and password");
  };

  return (
    <FormWrapper>
      <Form onSubmit={handleSubmit(getData)}>
        <Heading>Sign In</Heading>
        <InputWrapper>
          <Input
            {...register("email", { required: true })}
            type="email"
            placeholder="Email"
          />
        </InputWrapper>
        <InputWrapper>
          <Input
            {...register("password", { required: true })}
            type="password"
            placeholder="Password"
          />
        </InputWrapper>

        <InputWrapper>
          <Button>Log In</Button>
        </InputWrapper>
      </Form>
    </FormWrapper>
  );
};

export default SignIn;
