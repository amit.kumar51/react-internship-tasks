//import useFetch from "hooks/useFetch";
import { Table, TableHeader, TableRow } from "styles/components/Table";
import useFetch from "hooks/useFetch";
import { Heading } from "styles/components/Table";

const TableData = () => {
  const fetchedData = useFetch("https://jsonplaceholder.typicode.com/todos");

  return (
    <div>
      <Heading>
        <h1>Dashboard</h1>
      </Heading>
      <Table>
        <thead>
          <tr>
            <TableHeader>userId</TableHeader>
            <TableHeader>id</TableHeader>
            <TableHeader>title</TableHeader>
            <TableHeader>completed</TableHeader>
          </tr>
        </thead>
        {fetchedData.map((obj: any) => (
          <TableRow>
            <td>{obj.userId}</td>
            <td>{obj.id}</td>
            <td>{obj.title}</td>
            <td>{obj.completed ? "Completed" : "In Progress"}</td>
          </TableRow>
        ))}
      </Table>
    </div>
  );
};

export default TableData;
